//
//  PTimerAction.h
//  MatchTimer
//
//  Created by Timothy Pitt on 03/03/2017.
//  Copyright © 2017-2021 Timothy Pitt. All rights reserved.
//

#pragma once

#include "PMatchTimerAction.h"
#include "PPeriod.h"

class PStartAction : public PMatchTimerAction
{
public:
	PStartAction(unsigned long a_Index, PMatchTimer& a_Match);
	virtual void Redo();
	virtual void Undo();
};

class PPauseAction : public PMatchTimerAction
{
public:
	PPauseAction(unsigned long a_Index, PMatchTimer& a_Match);
	virtual void Redo();
	virtual void Undo();
};

class PContinueAction : public PMatchTimerAction
{
public:
	PContinueAction(unsigned long a_Index, PMatchTimer& a_Match);
	virtual void Redo();
	virtual void Undo();
private:
	ETimerState m_OldState;
	PPeriod::SAccumulatedTime m_OldTime;
};

class PTimeoutAction : public PMatchTimerAction
{
public:
	PTimeoutAction(unsigned long a_Index, PMatchTimer& a_Match);
	virtual void Redo();
	virtual void Undo();
private:
	bool m_bWasPaused;
};

class PFinishAction : public PMatchTimerAction
{
public:
	PFinishAction(unsigned long a_Index, PMatchTimer& a_Match);
	virtual void Redo();
	virtual void Undo();
private:
	bool m_bWasPaused;
};

