//
//  PMatchTimerSettings
//  Settings for different types of match
//
//  Copyright © 2021 Timothy Pitt, Vaquita Software. All rights reserved.
//
/*
	This file is part of the MatchTimer core library.

	MatchTimer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MatchTimer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MatchTimer in the COPYING file.
	If not, see <https://www.gnu.org/licenses/>.
*/

#include "PMatchTimerSettings.h"

#include "PBasicMatchSettings.h"
#include "PUniversalMatchSettings.h"


void PMatchTimerSettings::PopulateInitialDefaults()
{
	m_MatchSettingsList.clear();

	shared_ptr<PBasicMatchSettings> pMiniBasket(new PBasicMatchSettings );
	pMiniBasket->SetName("Mini-Basket");
	pMiniBasket->SetPeriodCount( 6 );
	pMiniBasket->SetPeriodTime( 8 );
	pMiniBasket->SetTimeout( 1 );
	pMiniBasket->SetRest( 1 );
	pMiniBasket->SetHalfTime( 5 );
	pMiniBasket->SetPeriodExtraAlarm( 1 );
	pMiniBasket->SetFinalPeriodExtraAlarm( 3 );
	pMiniBasket->SetPeriodBaseName( "P " );
	pMiniBasket->SetTimeoutName( "Timeout " );
	pMiniBasket->SetPeriodRestName( "Break " );
	pMiniBasket->SetHalfTimeName( "Half Time" );
	pMiniBasket->SetFinalPeriodRestName( "Full Time " );
	pMiniBasket->SetPauseName( "Pause" );
	m_MatchSettingsList.push_back( pMiniBasket );

	shared_ptr<PBasicMatchSettings> pFutsal(new PBasicMatchSettings );
	pFutsal->SetName("Futsal");
	pFutsal->SetPeriodCount( 2 );
	pFutsal->SetPeriodTime( 20 );
	pFutsal->SetTimeout( 1 );
	pFutsal->SetHalfTime( 15 );
	pFutsal->SetPeriodExtraAlarm( 1 );
	pFutsal->SetFinalPeriodExtraAlarm( 3 );
	pFutsal->SetPeriodBaseName( "P " );
	pFutsal->SetTimeoutName( "Timeout " );
	pFutsal->SetPeriodRestName( "Break " );
	pFutsal->SetHalfTimeName( "Half Time" );
	pFutsal->SetFinalPeriodRestName( "Full Time " );
	pFutsal->SetPauseName( "Pause" );
	m_MatchSettingsList.push_back( pFutsal );
}


unsigned int PMatchTimerSettings::MatchSettingsCount() const
{
	return m_MatchSettingsList.size();
}

const PMatchSettings *PMatchTimerSettings::MatchSettings(const int a_At) const
{
	STLMatchSettingsConstIter It( FindIterAt( a_At ) );
	if( It == m_MatchSettingsList.end() )
		return nullptr;
	return It->get();
}

void PMatchTimerSettings::InsertMatchSettings(const unsigned int a_Prev, const PMatchSettings &a_MatchSettings)
{
	unsigned int NewIndex(0);
	if( a_Prev < m_MatchSettingsList.size() - 1 )
		NewIndex = a_Prev + 1;
	else
		NewIndex = m_MatchSettingsList.size();
	STLMatchSettingsConstIter Next( FindIterAt(NewIndex) );
	shared_ptr<PMatchSettings> pNew( a_MatchSettings.Clone() );
	m_MatchSettingsList.insert( Next, pNew );
	MatchSettingsChanged( NewIndex );
}

unsigned int PMatchTimerSettings::AppendMatchSettings( const PMatchSettings& a_MatchSettings )
{
	shared_ptr<PMatchSettings> pNew( a_MatchSettings.Clone() );
	m_MatchSettingsList.push_back( pNew );
	const unsigned int NewIndex( m_MatchSettingsList.size() - 1 );
	MatchSettingsChanged( NewIndex );
	return NewIndex;
}

int PMatchTimerSettings::DeleteMatchSettings(const unsigned int a_I)
{
	if( a_I < m_MatchSettingsList.size() )
	{
		STLMatchSettingsIter It(FindIterAt(a_I));
		m_MatchSettingsList.erase(It);
	}
	if( a_I < m_MatchSettingsList.size() )
		return a_I;
	return static_cast<int>(m_MatchSettingsList.size()) - 1;
}

void PMatchTimerSettings::SetMatchSettings(const unsigned int a_At, const PMatchSettings &a_MatchSettings)
{
	STLMatchSettingsIter It( FindIterAt( a_At ) );
	if( It != m_MatchSettingsList.end() )
	{
		shared_ptr<PMatchSettings> pNew( a_MatchSettings.Clone() );
		*It = pNew;
		MatchSettingsChanged(a_At);
	}
}

//-----------------------------------

PMatchTimerSettings::STLMatchSettingsIter PMatchTimerSettings::FindIterAt(const int a_At )
{
	if( a_At < 0 )
		return m_MatchSettingsList.end();
	STLMatchSettingsIter It( m_MatchSettingsList.begin() );
	unsigned int Count( a_At );
	while( Count > 0 && It != m_MatchSettingsList.end() )
	{
		++It;
		--Count;
	}
	return It;
}

PMatchTimerSettings::STLMatchSettingsConstIter PMatchTimerSettings::FindIterAt(const int a_At ) const
{
	return const_cast<PMatchTimerSettings*>(this)->FindIterAt(a_At);
}

ostream &operator<<(ostream &a_Strm, const PMatchTimerSettings &a_Val)
{
	a_Strm << 1 << " ";
	a_Strm << a_Val.m_MatchSettingsList.size() << " ";
	for( PMatchTimerSettings::STLMatchSettingsConstIter It(a_Val.m_MatchSettingsList.begin()); It!=a_Val.m_MatchSettingsList.end(); ++It )
	{
		PMatchSettings::Write( a_Strm, **It);
	}
	return a_Strm;
}

istream &operator>>(istream &a_Strm, PMatchTimerSettings &a_Val)
{
	int Version(0);
	a_Strm >> Version;
	if( a_Strm.good() && Version == 1 )
	{
		int Count(0);
		a_Strm >> Count;
		if( a_Strm.good() && Count > 0 && Count < 100 )
		{
			a_Val.m_MatchSettingsList.clear();
			for( int I(0); I < Count; ++I )
			{
				PMatchSettings * pSettings = PMatchSettings::Read( a_Strm );
				if( pSettings )
				{
					shared_ptr< PMatchSettings > pNew( pSettings );
					a_Val.m_MatchSettingsList.push_back( pNew );
				}
			}
		}
	}
	return a_Strm;
}
