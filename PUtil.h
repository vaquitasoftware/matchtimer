//
//  PUtil
//
//  Utility functions
//
//  Copyright © 2021 Timothy Pitt, Vaquita Software. All rights reserved.
//
/*
	This file is part of the MatchTimer core library.

	MatchTimer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MatchTimer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MatchTimer in the COPYING file.
	If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>
#include <iostream>
using namespace std;

ostream& WriteQuotedString( ostream& a_rOutStream, const string& a_rString, const string& a_Ends = "\"\"" );
istream& ReadQuotedString( istream& a_rInStream, string& a_rString, const string& a_Ends = "\"\"" );
