//
//  SignalDefs.h
//
//  Created by Timothy Pitt on 10/02/2017.
//  Copyright © 2017, 2020 Timothy Pitt. All rights reserved.
//
/*
	This file is part of the MatchTimer core library.

	MatchTimer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MatchTimer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MatchTimer in the COPYING file.
	If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SignalDefs_h
#define SignalDefs_h

#include <list>
using std::list;

#include <boost/signals2/signal.hpp>
using boost::signals2::connection;

#include <string>
using std::string;

typedef std::list< connection > STLConnectionList;
typedef boost::signals2::signal<void ()> TSignal;
typedef boost::signals2::signal<void(unsigned int)> TSignalUInt;
typedef boost::signals2::signal<void(const string&)> TSignalString;

#endif /* SignalDefs_h */
