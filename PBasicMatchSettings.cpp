//
// Created by timot on 21/01/2021.
//

#include "PBasicMatchSettings.h"

#include <sstream>

unsigned int PBasicMatchSettings::PeriodCount() const
{
	return m_PeriodCount;
}

void PBasicMatchSettings::SetPeriodCount(unsigned int a_PeriodCount)
{
	m_PeriodCount = a_PeriodCount;
}

unsigned int PBasicMatchSettings::PeriodTime() const
{
	return m_PeriodTime;
}

void PBasicMatchSettings::SetPeriodTime(unsigned int a_PeriodTime)
{
	m_PeriodTime = a_PeriodTime;
}

unsigned int PBasicMatchSettings::Timeout() const
{
	return m_Timeout;
}

void PBasicMatchSettings::SetTimeout(unsigned int a_Timeout)
{
	m_Timeout = a_Timeout;
}

unsigned int PBasicMatchSettings::Rest() const
{
	return m_Rest;
}

void PBasicMatchSettings::SetRest(unsigned int a_Rest)
{
	m_Rest = a_Rest;
}

unsigned int PBasicMatchSettings::HalfTime() const
{
	return m_HalfTime;
}

void PBasicMatchSettings::SetHalfTime(unsigned int a_HalfTime)
{
	m_HalfTime = a_HalfTime;
}

unsigned int PBasicMatchSettings::PeriodExtraAlarm() const
{
	return m_PeriodExtraAlarm;
}

void PBasicMatchSettings::SetPeriodExtraAlarm(unsigned int a_PeriodExtraAlarm)
{
	m_PeriodExtraAlarm = a_PeriodExtraAlarm;
}

unsigned int PBasicMatchSettings::FinalPeriodExtraAlarm() const
{
	return m_FinalPeriodExtraAlarm;
}

void PBasicMatchSettings::SetFinalPeriodExtraAlarm(unsigned int a_FinalPeriodExtraAlarm)
{
	m_FinalPeriodExtraAlarm = a_FinalPeriodExtraAlarm;
}

const string &PBasicMatchSettings::PeriodBaseName() const
{
	return m_PeriodBaseName;
}

void PBasicMatchSettings::SetPeriodBaseName(const string &a_PeriodBaseName)
{
	m_PeriodBaseName = a_PeriodBaseName;
}

const string &PBasicMatchSettings::TimeoutName() const
{
	return m_TimeoutName;
}

void PBasicMatchSettings::SetTimeoutName(const string &a_TimeoutName)
{
	m_TimeoutName = a_TimeoutName;
}

const string &PBasicMatchSettings::PeriodRestName() const
{
	return m_PeriodRestName;
}

void PBasicMatchSettings::SetPeriodRestName(const string &a_PeriodRestName)
{
	m_PeriodRestName = a_PeriodRestName;
}

const string &PBasicMatchSettings::HalfTimeName() const
{
	return m_HalfTimeName;
}

void PBasicMatchSettings::SetHalfTimeName(const string &a_HalfTimeName)
{
	m_HalfTimeName = a_HalfTimeName;
}

const string &PBasicMatchSettings::FinalPeriodRestName() const
{
	return m_FinalPeriodRestName;
}

void PBasicMatchSettings::SetFinalPeriodRestName(const string &a_FinalPeriodRestName)
{
	m_FinalPeriodRestName = a_FinalPeriodRestName;
}

const string &PBasicMatchSettings::PauseName() const
{
	return m_PauseName;
}

void PBasicMatchSettings::SetPauseName(const string &a_PauseName)
{
	m_PauseName = a_PauseName;
}

//-----------------------------------------------------------------

PPlayPeriod::SPlaySettings PBasicMatchSettings::PeriodAt(const unsigned int a_At) const
{
	PPlayPeriod::SPlaySettings PeriodSettings;
	if( a_At < PeriodCount() )
	{
		bool bHalfTime( PeriodCount()%2==0 && a_At+1 == PeriodCount()/2 );
		bool bFinalPeriod(a_At + 1 == PeriodCount() );
		STLTimesVect RestAlarms = { seconds(15), seconds(0) };

		// Play Period
		std::ostringstream PeriodNameStrm;
		PeriodNameStrm << PeriodBaseName() << 1 + a_At;
		const string PlayName( PeriodNameStrm.str() );
		STLTimesVect PeriodAlarms = { seconds(30), seconds(10), seconds(0) };
		if( PeriodExtraAlarm() > 0 )
		{
			PeriodAlarms.insert( PeriodAlarms.begin(), minutes(PeriodExtraAlarm()));
		}
		if(bFinalPeriod && FinalPeriodExtraAlarm() > 0 )
		{
			PeriodAlarms.insert( PeriodAlarms.begin(), minutes(FinalPeriodExtraAlarm()));
		}
		PeriodSettings.m_PlayPeriod = PPeriod::SSettings( PlayName, minutes(PeriodTime()), PeriodAlarms );

		// Timeout Period
		PeriodSettings.m_TimeoutPeriod = PPeriod::SSettings( TimeoutName(), minutes(Timeout()), RestAlarms );

		// Rest Period
		string RestName;
		unsigned int RestMinutes(0);
		if( bHalfTime )
		{
			RestName = HalfTimeName();
			RestMinutes = HalfTime();
		}
		else if ( bFinalPeriod )
		{
			RestName = FinalPeriodRestName();
		}
		else
		{
			std::ostringstream Strm;
			Strm << PeriodRestName() << 1 + a_At;
			RestName = Strm.str();
			RestMinutes = Rest();
		}
		PeriodSettings.m_BreakPeriod = PPeriod::SSettings( RestName, minutes(RestMinutes), RestAlarms );

		// Pause name
		PeriodSettings.m_PauseName = m_PauseName;
	}
	return PeriodSettings;
}

void PBasicMatchSettings::Save(ostream &a_Strm) const
{
	a_Strm << 1 << " ";
	a_Strm << m_PeriodCount << " ";
	a_Strm << m_PeriodTime << " ";
	a_Strm << m_Timeout << " ";
	a_Strm << m_Rest << " ";
	a_Strm << m_HalfTime  << " ";
	a_Strm << m_PeriodExtraAlarm  << " ";
	a_Strm << m_FinalPeriodExtraAlarm  << " ";
	WriteQuotedString( a_Strm, m_PeriodBaseName );
	WriteQuotedString( a_Strm, m_TimeoutName );
	WriteQuotedString( a_Strm, m_PeriodRestName );
	WriteQuotedString( a_Strm, m_HalfTimeName );
	WriteQuotedString( a_Strm, m_FinalPeriodRestName );
	WriteQuotedString( a_Strm, m_PauseName );
}

void PBasicMatchSettings::Load(istream &a_Strm)
{
	int Version;
	a_Strm >> Version;
	if( Version == 1 )
	{
		a_Strm >> m_PeriodCount;
		a_Strm >> m_PeriodTime;
		a_Strm >> m_Timeout;
		a_Strm >> m_Rest;
		a_Strm >> m_HalfTime;
		a_Strm >> m_PeriodExtraAlarm;
		a_Strm >> m_FinalPeriodExtraAlarm;
		ReadQuotedString( a_Strm, m_PeriodBaseName );
		ReadQuotedString( a_Strm, m_TimeoutName );
		ReadQuotedString( a_Strm, m_PeriodRestName );
		ReadQuotedString( a_Strm, m_HalfTimeName );
		ReadQuotedString( a_Strm, m_FinalPeriodRestName );
		ReadQuotedString( a_Strm, m_PauseName );
	}
}
