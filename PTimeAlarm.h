//
//  PTimeAlarm
//  MatchTimer
//
//  This defines a simple time offset to the end of a Period
//  at which point an Alarm should be issued.
//
//  Created by Timothy Pitt on 06/02/2020.
//  Copyright © 2020 Timothy Pitt. All rights reserved.
//
/*
	This file is part of the MatchTimer core library.

	MatchTimer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MatchTimer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MatchTimer in the COPYING file.
	If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "SignalDefs.h"

#include <chrono>
using namespace std::chrono;

#include <vector>
using std::vector;

// TimeAlarmSettings :-
typedef vector<seconds> STLTimesVect;

class PTimeAlarm
{
public:
	PTimeAlarm();
	PTimeAlarm( const PTimeAlarm& a_Other );
	PTimeAlarm( const STLTimesVect& a_Offsets );
	PTimeAlarm& operator = ( const PTimeAlarm& a_Other );

	bool Triggered() const;

	bool Check( const seconds& a_Time );	
	void Acknowledge();
	void Reset();
	
	// signals
	TSignal AlarmChanged;
	
private:
	STLTimesVect m_Offsets;
	enum EState
	{
		EArmed,
		ETriggered,
	};
	EState m_State;
	STLTimesVect::size_type m_CheckStart;
};
