//
//  PMatchTimer
//  MatchTimer
//
//  Represent the match, in the form of a list of Play Periods
//
//  Copyright © 2020 Timothy Pitt. All rights reserved.
//
/*
	This file is part of the MatchTimer core library.

	MatchTimer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MatchTimer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MatchTimer in the COPYING file.
	If not, see <https://www.gnu.org/licenses/>.
*/

#include "PMatchTimer.h"
#include "PUniversalMatchSettings.h"
#include "PTimerAction.h"

#include <assert.h>

PMatchTimer::PMatchTimer()
{
	m_MatchTimerSettings.PopulateInitialDefaults();
	m_CurrentPeriod = m_PlayPeriodList.end();
	m_CurrentSetting = 0;
	SetMatchType();
}

PMatchTimer::~PMatchTimer()
{
}

void PMatchTimer::InitUI()
{ // Issue signals
	MatchTypeListChanged();
	MatchTypeChanged();
}


istream &PMatchTimer::ReadSettings(istream &a_Strm)
{
	a_Strm >> m_MatchTimerSettings;
	m_CurrentSetting = 0;
	int Version(0);
	a_Strm >> Version;
	if( a_Strm.good() && Version == 1 )
	{
		int Curr;
		a_Strm >> Curr;
		if( Curr < m_MatchTimerSettings.MatchSettingsCount() )
		{
			m_CurrentSetting = Curr;
		}
	}
	SetMatchType();
	return a_Strm;
}

ostream &PMatchTimer::WriteSettings(ostream &a_Strm)
{
	a_Strm << m_MatchTimerSettings;
	a_Strm << 1 << " ";
	a_Strm << m_CurrentSetting;
	// Later add the actual match state here, to protect against OS kills
	return a_Strm;
}


void PMatchTimer::SetMatchSettings(const unsigned int a_Index, const PMatchSettings &a_Settings)
{
	if( !CanChangeMatchType() )
		return;
	m_MatchTimerSettings.SetMatchSettings( a_Index, a_Settings );
	if( a_Index == m_CurrentSetting )
	{
		SetMatchType(a_Settings);
	}
}

int PMatchTimer::AddMatchSettings(const PMatchSettings &a_Settings)
{ // Adds to settings list
	if( !CanChangeMatchType() )
		return -1;
	// Select the new entry.
	// It's a bit of a side-effect functionality
	// but makes it consistent between platforms
	m_CurrentSetting = m_MatchTimerSettings.AppendMatchSettings( a_Settings );
	SetMatchType();
	return m_CurrentSetting;
}

bool PMatchTimer::DeleteMatchSettings(const unsigned int a_I)
{
	if( !CanChangeMatchType() )
		return false;
	// Select another entry.
	// It's a bit of a side-effect functionality
	// but makes behaviour consistent between platforms
	m_CurrentSetting = m_MatchTimerSettings.DeleteMatchSettings( a_I );
	MatchTypeListChanged();
	SetMatchType();
	return true;
}

void PMatchTimer::SetMatchType(const int a_Index )
{
	if( !CanChangeMatchType() || a_Index == m_CurrentSetting )
		return;
	m_CurrentSetting = a_Index;
	SetMatchType();
}

void PMatchTimer::SetMatchType()
{
	assert( CanChangeMatchType() );
	const PMatchSettings * pMatchSettings = m_MatchTimerSettings.MatchSettings(m_CurrentSetting);
	if( pMatchSettings )
	{
		SetMatchType(*pMatchSettings);
	} else {
		ClearMatchType();
	}
}

void PMatchTimer::ClearMatchType()
{
	assert( CanChangeMatchType() );
	m_TypeName.clear();
	m_PlayPeriodList.clear();
	m_CurrentPeriod = m_PlayPeriodList.begin();
	MatchTypeChanged();
}

void PMatchTimer::SetMatchType(const PMatchSettings& a_Setup )
{
	assert( CanChangeMatchType() );
	m_TypeName = a_Setup.Name();
	m_PlayPeriodList.clear();
	const int PeriodCount( a_Setup.PeriodCount() );
	for( int P(0); P < PeriodCount; ++P )
	{
		const PPlayPeriod::SPlaySettings Period = a_Setup.PeriodAt( P );
		if( Period.Valid() )
		{
			m_PlayPeriodList.push_back( PPlayPeriod( Period ) );
		}
	}
	m_CurrentPeriod = m_PlayPeriodList.begin();
	MatchTypeChanged();
}

PPlayPeriod * PMatchTimer::CurrentPeriod()
{
	if( m_CurrentPeriod == m_PlayPeriodList.end() )
		return NULL;
	return &( *m_CurrentPeriod );
}

// Commands
void PMatchTimer::StartPressed()
{
	if( !CanStart() )
		return;
	// If the PlayPeriod is Ready, then it can be started
	// Otherwise if it's Paused, it should be continued
	if( m_CurrentPeriod->State()==Paused || m_CurrentPeriod->State()==Timed_out )
		m_UndoStack.Add( new PContinueAction( m_UndoStack.NextIndex(), *this ) );
	else
		m_UndoStack.Add( new PStartAction( m_UndoStack.NextIndex(), *this ) );
}

void PMatchTimer::PausePressed()
{ // Pause if running or run if paused
	if( !CanPause() )
		return;
	if( m_CurrentPeriod->State() == Running )
		m_UndoStack.Add( new PPauseAction( m_UndoStack.NextIndex(), *this ) );
	else if( m_CurrentPeriod->State() == Paused )
		m_UndoStack.Add( new PContinueAction( m_UndoStack.NextIndex(), *this ) );
}

void PMatchTimer::TimeoutPressed()
{
	if( !CanTimeout() )
		return;
	m_UndoStack.Add( new PTimeoutAction( m_UndoStack.NextIndex(), *this ) );
}

void PMatchTimer::FinishPressed()
{
	if( !CanFinish() )
		return;
	m_UndoStack.Add( new PFinishAction( m_UndoStack.NextIndex(), *this ) );
}

void PMatchTimer::ResetPressed()
{
	if( !CanReset() )
		return;
	Reset();
}

void PMatchTimer::UndoPressed()
{
	if( !CanUndo() )
		return;
	m_UndoStack.Undo();
}

// Command enable
bool PMatchTimer::CanChangeMatchType() const
{ // Only before the match has started
	return m_PlayPeriodList.empty() ||
		m_CurrentPeriod==m_PlayPeriodList.begin()	&& m_CurrentPeriod->State()==Ready;
}
bool PMatchTimer::CanStart() const
{
	// You can press start
	// 1 - if the Iterator is valid AND
	// 2 - if the Current period can Start (i.e. Ready) OR
	// 2 - if the Current period is Paused OR timed_out OR
	// 4 - when the current period has finished, and you can advance to a new one.
	return m_CurrentPeriod!=m_PlayPeriodList.end() &&
		       (  m_CurrentPeriod->CanStart() ||
		          m_CurrentPeriod->CanContinue() ||
		          (m_CurrentPeriod->State()==Finished && next(m_CurrentPeriod)!=m_PlayPeriodList.end())
		       ); // Horrific
}
bool PMatchTimer::CanPause() const
{ // Enable if you can pause the current Period
	// Or if it's paused already
	return m_CurrentPeriod!=m_PlayPeriodList.end()
		&& ( m_CurrentPeriod->CanPause() || m_CurrentPeriod->State()==Paused );
}
bool PMatchTimer::CanTimeout() const
{
	return m_CurrentPeriod!=m_PlayPeriodList.end() && m_CurrentPeriod->CanTimeout();
}
bool PMatchTimer::CanFinish() const
{
	return m_CurrentPeriod!=m_PlayPeriodList.end() && m_CurrentPeriod->CanFinish();
}
bool PMatchTimer::CanUndo() const
{
	return m_UndoStack.CanUndo();
}
bool PMatchTimer::CanReset() const
{
	return true; //m_CurrentPeriod->State()==Finished && next(m_CurrentPeriod) == m_PlayPeriodList.end();
}
bool PMatchTimer::MatchFinished() const
{
	return m_CurrentPeriod->State()==Finished && next(m_CurrentPeriod) == m_PlayPeriodList.end();
}

//----------------------------------------------------------------------------
// Actual functions - called by the Action classes

void PMatchTimer::Start()
{
	if( m_CurrentPeriod == m_PlayPeriodList.end() )
	{
		m_CurrentPeriod = m_PlayPeriodList.begin();
		PeriodChanged();
	}
	else if( m_CurrentPeriod->State() == Finished )
	{
		m_CurrentPeriod->FinishRest();
		++m_CurrentPeriod;
		PeriodChanged();
	}
	if( m_CurrentPeriod != m_PlayPeriodList.end() )
	{
		m_CurrentPeriod->Start();
	}
}

void PMatchTimer::UndoStart()
{
	assert( m_CurrentPeriod != m_PlayPeriodList.end() );
	if( m_CurrentPeriod == m_PlayPeriodList.end() )
		return;
	m_CurrentPeriod->UndoStart();
	if( m_CurrentPeriod != m_PlayPeriodList.begin() )
	{
		--m_CurrentPeriod;
		m_CurrentPeriod->UndoFinishRest();
		PeriodChanged();
	}
}

void PMatchTimer::Pause()
{
	m_CurrentPeriod->Pause();
}
void PMatchTimer::UndoPause()
{
	m_CurrentPeriod->UndoPause();
}
PPeriod::SAccumulatedTime PMatchTimer::Continue()
{
	return m_CurrentPeriod->Continue();
}
void PMatchTimer::UndoContinue(const PPeriod::SAccumulatedTime &a_OldTime, const ETimerState a_OldState)
{
	m_CurrentPeriod->UndoContinue(a_OldTime, a_OldState);
}
bool PMatchTimer::Timeout()
{
	return m_CurrentPeriod->Timeout();
}
void PMatchTimer::UndoTimeout(bool a_bRestartPause)
{
	m_CurrentPeriod->UndoTimeout(a_bRestartPause);
}
bool PMatchTimer::Finish()
{
	const bool bWasPaused( m_CurrentPeriod->Finish() );
	if( MatchFinished() )
	{
		m_CurrentPeriod->FinishRest();
	}
	return bWasPaused;
}
void PMatchTimer::UndoFinish(bool a_bRestartPause)
{
	if( MatchFinished() )
	{
		m_CurrentPeriod->UndoFinishRest();
	}
	m_CurrentPeriod->UndoFinish( a_bRestartPause );
}

void PMatchTimer::Reset()
{
	for( TPlayPeriodList::iterator Period=m_PlayPeriodList.begin(); Period!=m_PlayPeriodList.end(); ++Period )
	{
		Period->Reset();
	}
	m_CurrentPeriod = m_PlayPeriodList.begin();
	m_UndoStack.Clear();
	PeriodChanged();
}
