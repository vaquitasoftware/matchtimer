//
//  PMatchTimerSettings
//  Settings for different types of match
//
//  Copyright © 2021 Timothy Pitt, Vaquita Software. All rights reserved.
//
/*
	This file is part of the MatchTimer core library.

	MatchTimer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MatchTimer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MatchTimer in the COPYING file.
	If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "PMatchSettings.h"

#include "SignalDefs.h"

#include <list>
using std::list;

class PMatchTimerSettings
{
public:
	void PopulateInitialDefaults();

	unsigned int MatchSettingsCount() const;
	const PMatchSettings * MatchSettings(const int a_At ) const;

	void InsertMatchSettings(const unsigned int a_Prev, const PMatchSettings& a_MatchSettings );
	unsigned int AppendMatchSettings( const PMatchSettings& a_MatchSettings );
	int DeleteMatchSettings( const unsigned int a_I);
	void SetMatchSettings(const unsigned int a_At, const PMatchSettings& a_MatchSettings );

	// signals
	TSignalUInt MatchSettingsChanged;

private:
	typedef list< shared_ptr<PMatchSettings> > STLMatchSettingsList;
	typedef STLMatchSettingsList::const_iterator STLMatchSettingsConstIter;
	typedef STLMatchSettingsList::iterator STLMatchSettingsIter;
	STLMatchSettingsList m_MatchSettingsList;

	STLMatchSettingsIter FindIterAt(const int a_At );
	STLMatchSettingsConstIter FindIterAt(const int a_At ) const;

	friend ostream& operator << ( ostream& a_Strm, const PMatchTimerSettings& a_Val );
	friend istream& operator >> ( istream& a_Strm, PMatchTimerSettings& a_Val );
};

