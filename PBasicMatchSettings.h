//
//  PBasicMatchSettings
//  Has a short set of values, which can be used to
//  generate the fully flexible settings of the base class.
//
//  Copyright © 2021 Timothy Pitt, Vaquita Software. All rights reserved.
//
/*
	This file is part of the MatchTimer core library.

	MatchTimer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MatchTimer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MatchTimer in the COPYING file.
	If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "PMatchSettings.h"


class PBasicMatchSettings : public PMatchSettings
{
public:
	PBasicMatchSettings()
	: m_PeriodCount(2),
	m_PeriodTime(1),
	m_Timeout(0),
	m_Rest(0),
	m_HalfTime(1),
	m_PeriodExtraAlarm(0),
	m_FinalPeriodExtraAlarm(0)
	{}
	
	virtual PBasicMatchSettings * Clone() const
	{ return new PBasicMatchSettings( *this ); }

	virtual unsigned int PeriodCount() const;
	void SetPeriodCount(unsigned int a_PeriodCount);
	unsigned int PeriodTime() const;
	void SetPeriodTime(unsigned int a_PeriodTime);
	unsigned int Timeout() const;
	void SetTimeout(unsigned int a_Timeout);
	unsigned int Rest() const;
	void SetRest(unsigned int a_Rest);
	unsigned int HalfTime() const;
	void SetHalfTime(unsigned int a_HalfTime);
	unsigned int PeriodExtraAlarm() const;
	void SetPeriodExtraAlarm(unsigned int a_PeriodExtraAlarm);
	unsigned int FinalPeriodExtraAlarm() const;
	void SetFinalPeriodExtraAlarm(unsigned int a_FinalPeriodExtraAlarm);

	const string &PeriodBaseName() const;
	void SetPeriodBaseName(const string &a_PeriodBaseName);
	const string &TimeoutName() const;
	void SetTimeoutName(const string &a_TimeoutName);
	const string &PeriodRestName() const;
	void SetPeriodRestName(const string &a_PeriodRestName);
	const string &HalfTimeName() const;
	void SetHalfTimeName(const string &a_HalfTimeName);
	const string &FinalPeriodRestName() const;
	void SetFinalPeriodRestName(const string &a_FinalPeriodRestName);
	const string &PauseName() const;
	void SetPauseName(const string &a_PauseName);

	virtual PPlayPeriod::SPlaySettings PeriodAt(const unsigned int a_At) const;

protected:
	virtual EMatchSettings ID() const
	{ return PMatchSettings::EBasicMatchSettingsID; }

private:
	unsigned int m_PeriodCount;
	// Period times - in minutes
	unsigned int m_PeriodTime;
	unsigned int m_Timeout;
	unsigned int m_Rest;
	unsigned int m_HalfTime; // Only used if PeriodCount is even
	// Extra Warning times for periods - in minutes
	// You always get an alarm at 30, 10 and 0s before the end of each period.
	// Timeouts and Breaks have alarms at 15 and 0s always.
	unsigned int m_PeriodExtraAlarm;
	unsigned int m_FinalPeriodExtraAlarm;

	string m_PeriodBaseName;
	string m_TimeoutName;
	string m_PeriodRestName;
	string m_HalfTimeName;
	string m_FinalPeriodRestName;
	string m_PauseName;

private:
	virtual void Save( ostream& a_Strm ) const;
	virtual void Load( istream& a_Strm );

};
