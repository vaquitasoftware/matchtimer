//
//  PMatchSettings
//  ABC for settings to define Matches as a series of PlayPeriods
//  Also includes factory Create() with derived class ID enums
//  It's a bit ugly but keeps things in one place when you add a new class
//
//  Copyright © 2021 Timothy Pitt, Vaquita Software. All rights reserved.
//
/*
	This file is part of the MatchTimer core library.

	MatchTimer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MatchTimer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MatchTimer in the COPYING file.
	If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "PPlayPeriod.h"

#include <string>
using std::string;

#include <iostream>
using std::ostream;
using std::istream;

class PMatchSettings
{
public:
	PMatchSettings() {}
	virtual ~PMatchSettings() {}
	virtual PMatchSettings * Clone() const = 0;

	const string& Name() const
	{ return m_Name; }
	void SetName( const string& a_Name )
	{ m_Name = a_Name; }

	virtual unsigned int PeriodCount() const = 0;
	virtual PPlayPeriod::SPlaySettings PeriodAt(const unsigned int a_At) const = 0;

	static void Write( ostream& a_Strm, const PMatchSettings& a_Val );
	static PMatchSettings * Read( istream& a_Strm );

protected:
	enum EMatchSettings
	{
		EUnknown,
		EBasicMatchSettingsID,
		EUniversalMatchSettingsID,
	};
	virtual EMatchSettings ID() const = 0;

private:
	string m_Name;

	static PMatchSettings * Create( unsigned int a_ID );
	virtual void Save( ostream& a_Strm ) const = 0;
	virtual void Load( istream& a_Strm ) = 0;
};


