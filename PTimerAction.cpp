//
//  PMatchTimerAction.cpp
//  MatchTimer
//
//  Created by Timothy Pitt on 03/03/2017.
//  Copyright © 2017-2021 Timothy Pitt. All rights reserved.
//

#include "PTimerAction.h"
#include "PMatchTimer.h"

//-----------------------------------------------------------

PStartAction::PStartAction(unsigned long a_Index, PMatchTimer& a_Match)
: PMatchTimerAction( a_Index, EStartAction, a_Match)
{}

void PStartAction::Redo()
{
	m_Match.Start();
}

void PStartAction::Undo()
{
	m_Match.UndoStart();
}

//-----------------------------------------------------------
PPauseAction::PPauseAction(unsigned long a_Index, PMatchTimer& a_Match)
: PMatchTimerAction( a_Index, EPauseAction, a_Match)
{}

void PPauseAction::Redo()
{
	m_Match.Pause();
}

void PPauseAction::Undo()
{
	m_Match.UndoPause();
}

//-----------------------------------------------------------
PContinueAction::PContinueAction(unsigned long a_Index, PMatchTimer &a_Match)
: PMatchTimerAction( a_Index, EContinueAction, a_Match)
{}

void PContinueAction::Redo()
{
	m_OldState = m_Match.CurrentPeriod()->State();
	m_OldTime = m_Match.Continue();
}

void PContinueAction::Undo()
{
	m_Match.UndoContinue(m_OldTime, m_OldState);
}

//-----------------------------------------------------------
PTimeoutAction::PTimeoutAction(unsigned long a_Index, PMatchTimer& a_Match)
: PMatchTimerAction( a_Index, ETimeoutAction, a_Match),
  m_bWasPaused( false )
{}

void PTimeoutAction::Redo()
{
	m_bWasPaused = m_Match.Timeout();
}

void PTimeoutAction::Undo()
{
	m_Match.UndoTimeout(m_bWasPaused);
}

//-----------------------------------------------------------

PFinishAction::PFinishAction(unsigned long a_Index, PMatchTimer& a_Match)
: PMatchTimerAction( a_Index, EFinishAction, a_Match),
  m_bWasPaused( false )
{}

void PFinishAction::Redo()
{
	m_bWasPaused = m_Match.Finish();
}

void PFinishAction::Undo()
{
	m_Match.UndoFinish(m_bWasPaused);
}

//-----------------------------------------------------------
