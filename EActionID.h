//
//  EActionID.h
//  TeleScore
//
//  Created by Timothy Pitt on 24/02/2017.
//  Copyright © 2017 Timothy Pitt. All rights reserved.
//

#ifndef EAction_h
#define EAction_h

enum EActionID
{
	ENoAction,
	EStartAction,
	EPauseAction,
	EContinueAction,
	ETimeoutAction,
	EFinishAction,
	// n.b. use E<classname> to match P<classname> so the Factory MACROS work :-)
	
	EActionCount // Always last !
};

#endif /* EAction_h */
