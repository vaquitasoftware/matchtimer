MatchTimer core
===============

Introduction
------------
The intention of MatchTimer is to provide the logic allowing you to build timer applications for different sports. This initial version is designed to work for "mini" basketball, where there are six short periods. The intention is for it to be an aid for parents that volunteer to be on the scoring table.

The core code allows you to define a match as consisting of any number of periods. Each playing period has it's own defined duration, and is followed by a rest period of it's own duration. The playing period can be interrupted by a pause, or by timeout period. Any of these periods can have a number of alarm times used to alert the user at key moments - e.g. a few seconds before the end of a period.

Contact
-------

Feel free to get into contact with any bug fixes or suggestions.

tim@vaquita.co.uk

Build Notes
===

Boost
------------
The code needs to link with the boost/signals2/signal.hpp (see https://www.boost.org/doc/libs/1_72_0/doc/html/signals2.html). It is tested with boost 1.63.

Integration
----

Match Times
------------

The Periods which make up the timings for a match are defined using a series of Settings structs which are passed to the MatchTime class constructor. You can find an example in the global defined in PMatchTimer.cpp.

Time Signal
------------

Your native system code must provide an timer signal to the MatchTimer. To do this it should respond to the current PPlayPeriod::StartUpdateTimer and StopUpdateTimer signals to Start/Stop a periodic system timer. When the system timer fires, it should call PPlayPeriod::UpdateTime(). The accuracy of the timer is not important - the core code checks the actual time on the system clock - but if the periods are too long the time updates could appear jittery to the user. PPlayPeriod::StartUpdateTimer passes a suggested timer update period of 100ms.

UI Controls
------------

UI control signals to start play, pause, etc. are simply integrated by calling the corresponding MatchTimer functions. MatchTimer also provides indicators of the "availability" of the functions, which can be used to enable/disable UI components

UI Updates
------------

Whenever an internal state changes, a signal is emitted from the object instance affected e.g. PPlayPeriod::TimeChanged(). You should bind these signals to a funtion which will update the UI. The actual state of the item in question needs to be determined by calling the appropriate function e.g. PPlayPeriod::RemainingTime()