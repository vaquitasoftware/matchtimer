//
//  PMatchTimer
//  MatchTimer
//
//  Represent the match, in the form of a list of Play Periods
//
//  Copyright © 2020 Timothy Pitt. All rights reserved.
//
/*
	This file is part of the MatchTimer core library.

	MatchTimer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MatchTimer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MatchTimer in the COPYING file.
	If not, see <https://www.gnu.org/licenses/>.
*/

/*
	This is basically a list of PlayPeriods
	Only one Period is ever Active
	This class handles 
	- changing the Periods as they progress
	- routing of messages to/from the UI
*/

#pragma once

#include "PPlayPeriod.h"
#include "PUniversalMatchSettings.h"
#include "PMatchTimerSettings.h"
#include "PUndoStack.h"
#include "SignalDefs.h"

#include <list>
using std::list;

class PMatchTimerAction;

class PMatchTimer
{
public:
	PMatchTimer();
	~PMatchTimer();

	void InitUI();

	istream & ReadSettings( istream &a_Strm );
	ostream & WriteSettings( ostream &a_Strm );
	const PMatchTimerSettings &MatchTimerSettings() const
	{	return m_MatchTimerSettings; }
	PMatchTimerSettings &MatchTimerSettings()
	{	return m_MatchTimerSettings; }
	
	void SetMatchSettings( const unsigned int a_Index, const PMatchSettings& a_Settings );
	int AddMatchSettings(const PMatchSettings &a_Settings );
	bool DeleteMatchSettings(const unsigned int a_I);
	void SetMatchType(const int a_Index );
	int CurrentMatchType() const
	{ return m_CurrentSetting; }
	string TypeName() const
	{ return m_TypeName; }
	
private:
	void SetMatchType();
	void ClearMatchType();
	void SetMatchType(const PMatchSettings& a_Setup );

public:

	// should be const, but then we have problems connection to non-const signals...
	PPlayPeriod * CurrentPeriod();
	
	// Commands
	// or rather Button Presses - should read StartButtonPressed() etc.
	// redirected depending on which is active
	void StartPressed();
	void PausePressed();
	void TimeoutPressed();
	void FinishPressed();
	void UndoPressed();
	void ResetPressed();

	// Whether commands will do anything useful or not
	// i.e. Button enable
	bool CanChangeMatchType() const;
	bool CanStart() const;
	bool CanPause() const;
	bool CanTimeout() const;
	bool CanFinish() const;
	bool CanUndo() const;
	bool CanReset() const;
	bool MatchFinished() const;
	
	// signals
	TSignal PeriodChanged;
	TSignal MatchTypeListChanged;
	TSignal MatchTypeChanged;

public:
	// Actual functions - called by the Action classes
	void Start();
	void UndoStart();
	void Pause();
	void UndoPause();
	PPeriod::SAccumulatedTime Continue();
	void UndoContinue(const PPeriod::SAccumulatedTime& a_OldTime, const ETimerState a_OldState );
	bool Timeout();
	void UndoTimeout(bool a_bRestartPause);
	bool Finish();
	void UndoFinish(bool a_bRestartPause);
	void Reset();

private:
	PMatchTimerSettings m_MatchTimerSettings;
	int m_CurrentSetting;

	string m_TypeName;
	typedef list< PPlayPeriod > TPlayPeriodList;
	TPlayPeriodList m_PlayPeriodList;
	TPlayPeriodList::iterator m_CurrentPeriod;
	PUndoStack m_UndoStack;
};
