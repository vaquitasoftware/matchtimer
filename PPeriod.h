//
//  PPeriod
//  Performs the timing for a period
//
//  Copyright © 2017, 2020, 2021 Timothy Pitt. All rights reserved.
//
/*
	This file is part of the MatchTimer core library.

	MatchTimer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MatchTimer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MatchTimer in the COPYING file.
	If not, see <https://www.gnu.org/licenses/>.
*/

/* TODO
 * There's a strong argument to add Continue()
 * instead of using Start() after a Pause()
 * thus making Start() simpler
 * and fixing wrong behaviour after UndoStart() is called
 * after Pause() then Start()
*/

#pragma once

#include "ETimerState.h"
#include "SignalDefs.h"
#include "PTimeAlarm.h"
#include "PUtil.h"

#include <string>
using std::string;
#include <chrono>
using namespace std::chrono;
#include <iostream>
using std::ostream;
using std::istream;

class PPeriod
{
public:
	struct SSettings 
	{
		string m_Name;
		seconds m_Duration;
		STLTimesVect m_AlarmTimes;

		SSettings( const string& a_Name = string(), const seconds& a_Duration = seconds::zero(),
						const STLTimesVect& a_AlarmTimes = STLTimesVect() )
			: m_Name( a_Name ),
			m_Duration( a_Duration ),
			m_AlarmTimes( a_AlarmTimes )
		{}

		bool Valid() const
		{ return m_Duration.count() > 0; }

		friend ostream& operator << ( ostream& a_Strm, const SSettings& a_Val )
		{
			a_Strm << 1 << " ";
			WriteQuotedString( a_Strm, a_Val.m_Name );
			a_Strm << a_Val.m_Duration.count() << " ";
			a_Strm << a_Val.m_AlarmTimes.size() << " ";
			for( STLTimesVect::const_iterator It( a_Val.m_AlarmTimes.begin()); It !=a_Val.m_AlarmTimes.end(); ++It )
			{
				a_Strm << It->count() << " ";
			}
			return a_Strm;
		}
		friend istream& operator >> ( istream& a_Strm, SSettings& a_Val )
		{
			int Version(0);
			a_Strm >> Version;
			if( a_Strm.good() && Version == 1 )
			{
				ReadQuotedString( a_Strm, a_Val.m_Name );
				long Duration;
				short AlarmCount;
				a_Strm >> Duration >> AlarmCount;
				if( a_Strm.good() && Duration < 24*60*60 && AlarmCount < 100 )
				{
					a_Val.m_Duration = seconds(Duration);
					a_Val.m_AlarmTimes.resize( AlarmCount, seconds(0) );
					for( STLTimesVect::iterator It( a_Val.m_AlarmTimes.begin()); It !=a_Val.m_AlarmTimes.end(); ++It )
					{
						long Time;
						a_Strm >> Time;
						if( a_Strm.good() && Time < 24*60*60 )
						{
							*It = seconds( Time );
						}
					}
				}
			}
			return a_Strm;
		}
	};

	struct SAccumulatedTime
	{
		SAccumulatedTime()
		{
			Clear();
		}
		void Clear()
		{
			m_TimerStart = system_clock::time_point( system_clock::duration( 0 ) );
			m_MatchSeconds = seconds::zero();
			m_TimeCheckRemainder = microseconds::zero();
		}
		system_clock::time_point m_TimerStart;
		seconds m_MatchSeconds;
		microseconds m_TimeCheckRemainder;
	};

	PPeriod( const SSettings& a_Settings );
	PPeriod( const PPeriod& a_Other );
	virtual ~PPeriod();
	PPeriod& operator=( const PPeriod& a_Other );

	string Name() const { return m_Settings.m_Name; }
	void Name( string val ) { m_Settings.m_Name = val; }
	seconds Duration() const { return m_Settings.m_Duration; }
	void Duration( seconds val ) { m_Settings.m_Duration = val; }
	PTimeAlarm& Alarm() { return m_Alarm; }

	time_t StartTime() const;
	time_t TimeNow() const;

	void Synchronize( const time_t& a_Start, const time_t& a_Sent, const unsigned long& a_Elapsed );

	ETimerState State() const;
	seconds RemainingTime() const; // finally, these really should be const
	seconds ElapsedTime() const;

	// Controls
	void SetReady(); // kept for convenience...
	virtual void Start();
	virtual void UndoStart();
	virtual void Pause();
 	virtual void UndoPause();
	virtual PPeriod::SAccumulatedTime Continue();
	virtual void UndoContinue(const SAccumulatedTime &a_OldTimes, const ETimerState a_OldState);
	virtual bool Finish();
	virtual void UndoFinish(bool a_bRestartPause);
	virtual void Reset();
	
	// Whether commands will do anything useful or not
	// (for UI)
	bool CanStart() const;
	bool CanPause() const;
	virtual bool CanContinue() const;
	bool CanFinish() const;
	// bool CanUndo() const; - proposal...
	bool CanReset() const;

	void StartUpdates() const;

	// signals
	TSignal TimeChanged;
	TSignal StateChanged; // include state ?!
	TSignalUInt StartUpdateTimer;
	TSignal StopUpdateTimer;

	// Timer callback
	// ideally should be passed as a parameter of StartUpdateTimer() !
	virtual void UpdateTime();

protected:
	void SetState( ETimerState a_State );
	void DoPause();
	void DoUndoPause();

private:
	SSettings m_Settings;
	PTimeAlarm m_Alarm;

	ETimerState m_State;

	system_clock::time_point m_StartTime;

	SAccumulatedTime m_AccumulatedTime;
	bool AccumulateTime();

public:
	static string DisplayTimeStr( const seconds a_Seconds );
	static string DisplayTimeStr( const time_t a_Time );

};

