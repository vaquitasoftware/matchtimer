//
//  PAction.h
//  MatchTimer
//
//  Created by Timothy Pitt on 06/02/2017.
//  Copyright © 2017-2021 Timothy Pitt. All rights reserved.
//

#pragma once


class PAction
{
public:
	PAction( unsigned long a_Index )
	: m_Index( a_Index )
	{}
	virtual ~PAction(){}
	
	unsigned long Index() const { return m_Index; } // also for Telescore if you ask me...
	
	virtual void Redo() = 0;
	// Telescore specific surely ! virtual string RedoMessage() const = 0;
	virtual void Undo() = 0;
	// Telescore specific surely ! virtual string UndoMessage() const = 0;

private:
	const unsigned long m_Index;
};


