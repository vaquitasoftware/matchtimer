//
//  PUniversalMatchSettings
//  Period settings to define any type of match
//  Just a list of SPlayPeriodSettings
//
//  Copyright © 2021 Timothy Pitt, Vaquita Software. All rights reserved.
//
/*
	This file is part of the MatchTimer core library.

	MatchTimer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MatchTimer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MatchTimer in the COPYING file.
	If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "PMatchSettings.h"

#include <list>
using std::list;
class PUniversalMatchSettings : public PMatchSettings
{
public:
	virtual PUniversalMatchSettings * Clone() const
	{ return new PUniversalMatchSettings(*this ); }

	virtual unsigned int PeriodCount() const;
	virtual PPlayPeriod::SPlaySettings PeriodAt(const unsigned int a_At) const;

	void AddPeriod(const unsigned int a_Prev );
	void SetPeriod(const unsigned int a_At, PPlayPeriod::SPlaySettings a_PeriodSettings );
	void AppendPeriod( PPlayPeriod::SPlaySettings a_PeriodSettings );

protected:
	virtual EMatchSettings ID() const
	{ return PMatchSettings::EUniversalMatchSettingsID; }

private:
	typedef list< PPlayPeriod::SPlaySettings > STLPeriodsSettingList;
	typedef STLPeriodsSettingList::const_iterator STLPeriodsConstIter;
	typedef STLPeriodsSettingList::iterator STLPeriodsIter;
	STLPeriodsSettingList m_PeriodSettings;

	STLPeriodsIter FindIterAt(const unsigned int a_At );
	STLPeriodsConstIter FindIterAt(const unsigned int a_At ) const;

	virtual void Save( ostream& a_Strm ) const;
	virtual void Load( istream& a_Strm );
};
