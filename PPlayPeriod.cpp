//
//  PPlayPeriod
//  A period which can be interrupted by a pause or timeout
//
//  Copyright © 2020 Timothy Pitt. All rights reserved.
//
/*
	This file is part of the MatchTimer core library.

	MatchTimer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MatchTimer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MatchTimer in the COPYING file.
	If not, see <https://www.gnu.org/licenses/>.
*/

/*
	This is a Period, which can be interrupted by non-playing periods,
	(only one at a time)
	which go onto a list for future reference (stats)
	Only one Period is ever Active, this one, or the non-play one.
	This class handles creation of the non-play periods
	and the routing of messages to/from the UI
*/

#include "PPlayPeriod.h"

PPlayPeriod::PPlayPeriod( const SPlaySettings& a_Settings )
	: PPeriod( a_Settings.m_PlayPeriod ),
	m_TimeoutSettings( a_Settings.m_TimeoutPeriod ),
	m_BreakSettings( a_Settings.m_BreakPeriod ),
	m_PauseName( a_Settings.m_PauseName )
{
}

PPlayPeriod::PPlayPeriod( const PPlayPeriod& a_Other )
	: PPeriod( a_Other ),
	m_TimeoutSettings( a_Other.m_TimeoutSettings ),
	m_BreakSettings( a_Other.m_BreakSettings ),
	m_RestPeriods( a_Other.m_RestPeriods ),
	m_PauseName( a_Other.m_PauseName )
{}

PPlayPeriod::~PPlayPeriod()
{
}

PPeriod * PPlayPeriod::RestPeriod()
{
	if( m_RestPeriods.empty() )
		return NULL;
	return &m_RestPeriods.back();
}

void PPlayPeriod::Start()
{ // Only allow this if the period hasn't started already
	// Then Start the main play period
	if( !CanStart() )
		return;
	PPeriod::Start();
}

void PPlayPeriod::UndoStart()
{	// Undo the last non-play period Finish
	// UndoStart of the main play period
	if( State() != Running )
		return;
	PPeriod::UndoStart();
	assert( m_RestPeriods.empty() );
}

void PPlayPeriod::Pause()
{ // Pause
	// Add a Pause Period to the non-play list and start it running
	assert( (m_RestPeriods.empty() || m_RestPeriods.back().State()==Finished ) && "Pause requested, but there's already a non-play period running !" );
	if( !CanPause() )
		return;
	m_RestPeriods.push_back( PPeriod( SSettings( m_PauseName ) ) );
	RestChanged();
	PPeriod::Pause();
	m_RestPeriods.back().Start();
}

void PPlayPeriod::UndoPause()
{ // UndoPause
	// Remove last non-play period
	if( State() != Paused )
		return;
	m_RestPeriods.pop_back(); // destructor calls Finish()
	RestChanged();
	PPeriod::DoUndoPause();
	SetState( Running );
}

PPeriod::SAccumulatedTime PPlayPeriod::Continue()
{ // Stop the pause timer, continue the main play period
	assert( !m_RestPeriods.empty() );
	assert( m_RestPeriods.back().State() == Running && "Continuing Play but with a non-play period which was not Running !" );
	m_RestPeriods.back().Finish();
	RestChanged();
	return PPeriod::Continue();
}

void PPlayPeriod::UndoContinue(const PPeriod::SAccumulatedTime &a_OldTimes, const ETimerState a_OldState)
{ // Undo the main play timer, restart the paused period
	PPeriod::UndoContinue(a_OldTimes, a_OldState);
	assert( !m_RestPeriods.empty() );
	assert( m_RestPeriods.back().State() == Finished && "UnFinishing a non-play period which was not Finished !" );
	m_RestPeriods.back().UndoFinish(false);
	RestChanged();
}

bool PPlayPeriod::Finish()
{	// If paused, Finish the current Pause period
	// Finish the current PlayPeriod
	// Add a Break Period to the non-play list and start it running
	if( !CanFinish() )
		return false;
	EnsureLastRestStopped();
	const bool bWasPaused( PPeriod::Finish() );
	m_RestPeriods.push_back( PPeriod( m_BreakSettings ) );
	RestChanged();
	m_RestPeriods.back().Start();
	return bWasPaused;
}

void PPlayPeriod::UndoFinish(bool a_bRestartPause)
{ // UndoFinish
	// Remove last non-play period
	// Restart the previous pause period
	if( State() != Finished )
		return;
	m_RestPeriods.pop_back();
	if( a_bRestartPause )
	{
		assert( m_RestPeriods.back().State() == Finished );
		m_RestPeriods.back().UndoFinish(false);
	}
	PPeriod::UndoFinish(a_bRestartPause);
	RestChanged();
}

void PPlayPeriod::Reset() {
	if( !CanReset() )
		return;
	if( !m_RestPeriods.empty() && m_RestPeriods.back().State()!=Finished )
	{
		m_RestPeriods.back().Finish();
	}
	m_RestPeriods.clear();
	PPeriod::Reset();
}

bool PPlayPeriod::CanContinue() const
{
	return PPeriod::CanContinue() || State()==Timed_out;
}

bool PPlayPeriod::Timeout()
{ // If paused when a Timeout is called
	// Finish the pause
	// Add a Timeout Period to the non-play list and start it running
	if( !CanTimeout() )
		return false;
	const bool bWasPaused( EnsureLastRestStopped() );
	m_RestPeriods.push_back( PPeriod( m_TimeoutSettings ) );
	RestChanged();
	if( !bWasPaused )
		PPeriod::DoPause();
	m_RestPeriods.back().Start();
	SetState( Timed_out );
	return bWasPaused;
}

void PPlayPeriod::UndoTimeout(bool a_bRestartPause)
{ // Remove last non-play period
	if( State() != Timed_out )
		return;
	m_RestPeriods.pop_back();// Calls Finish in destructor
	RestChanged();
	if( a_bRestartPause )
	{
		assert( m_RestPeriods.back().State() == Finished );
		m_RestPeriods.back().UndoFinish(false);
		SetState( Paused );
	}
	else
	{
		PPeriod::DoUndoPause();
		SetState( Running );
	}
}

void PPlayPeriod::FinishRest()
{	// Finish the Rest Period
	// which only runs after PlayPeriod has finished
	if( State() != Finished )
		return;
	assert( !m_RestPeriods.empty() && m_RestPeriods.back().State()==Running && "There's no non-play period running !" );
	EnsureLastRestStopped();
}

void PPlayPeriod::UndoFinishRest()
{	// UndoFinish for the Rest Period
	// which only runs after PlayPeriod has finished
	if( State() != Finished )
		return;
	m_RestPeriods.back().UndoFinish(false);
}

bool PPlayPeriod::CanTimeout() const
{
	return m_TimeoutSettings.Valid() && (State()==Running || State()==Paused);
}

void PPlayPeriod::UpdateTime()
{
	if( State() == Running )
		PPeriod::UpdateTime();
	else
	{
		PPeriod * pRestPeriod = RestPeriod();
		if( pRestPeriod )
			pRestPeriod->UpdateTime();
	}
}

bool PPlayPeriod::EnsureLastRestStopped()
{
	if( !m_RestPeriods.empty() && m_RestPeriods.back().State()==Running )
	{
		m_RestPeriods.back().Finish();
		return true;
	}
	return false;
}
