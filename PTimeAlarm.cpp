//
//  PTimeAlarm.cpp
//  MatchTimer
//
//  This defines a simple time offset to the end of a Period
//  at which point an Alarm should be issued.
//
//  Created by Timothy Pitt on 06/02/2020.
//  Copyright © 2020 Timothy Pitt. All rights reserved.
//
/*
	This file is part of the MatchTimer core library.

	MatchTimer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MatchTimer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MatchTimer in the COPYING file.
	If not, see <https://www.gnu.org/licenses/>.
*/


#include "PTimeAlarm.h"


PTimeAlarm::PTimeAlarm()
: m_State( EArmed ),
m_CheckStart(0)
{
}

PTimeAlarm::PTimeAlarm(const PTimeAlarm& a_Other )
: m_Offsets( a_Other.m_Offsets ),
m_State( a_Other.m_State ),
m_CheckStart( a_Other.m_CheckStart )
{
}

PTimeAlarm::PTimeAlarm(const STLTimesVect& a_Offsets)
: m_Offsets( a_Offsets ),
m_State( EArmed ),
m_CheckStart(0)
{
}

PTimeAlarm& PTimeAlarm::operator = ( const PTimeAlarm& a_Other )
{
	m_Offsets = a_Other.m_Offsets;
	m_State = a_Other.m_State;
	m_CheckStart = a_Other.m_CheckStart;
	return *this;
}

bool PTimeAlarm::Triggered() const
{
	return m_State==ETriggered;
}

bool PTimeAlarm::Check(const seconds& a_Time)
{	// Check if an alarm is triggered
	// return true if one was triggered during the check
	// Keep the TriggeredIndex (m_CheckStart) up to date
	if( m_CheckStart<m_Offsets.size() && a_Time <= m_Offsets.at(m_CheckStart) )
	{
		++m_CheckStart;
		m_State = ETriggered;
		AlarmChanged();
		return true;
	}
	return false;
}

void PTimeAlarm::Acknowledge() {
	if( m_State != EArmed )
	{
		m_State = EArmed;
		AlarmChanged();
	}
	
}

void PTimeAlarm::Reset() {
	m_State = EArmed;
	m_CheckStart = 0;
}

