//
//  PPeriod
//  Performs the timing for a period
//
//  Copyright © 2017, 2020 Timothy Pitt. All rights reserved.
//
/*
	This file is part of the MatchTimer core library.

	MatchTimer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MatchTimer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MatchTimer in the COPYING file.
	If not, see <https://www.gnu.org/licenses/>.
*/


#include "PPeriod.h"

#include <sstream>
using std::ostringstream;
#include <iomanip>
#include <assert.h>

PPeriod::PPeriod( const SSettings& a_Settings )
	: m_Settings( a_Settings ),
	m_Alarm( a_Settings.m_AlarmTimes ),
	m_State( Ready )
{
}

PPeriod::PPeriod( const PPeriod& a_Other )
{ 
	*this = a_Other;
}

PPeriod::~PPeriod()
{
	Finish();
}

PPeriod& PPeriod::operator=( const PPeriod& a_Other )
{	// Caution - the undo stack does not reference this object
	if( this == &a_Other )
		return *this;

	m_Settings = a_Other.m_Settings;
	m_Alarm = a_Other.m_Alarm;
	m_State = a_Other.m_State;
	m_AccumulatedTime = a_Other.m_AccumulatedTime;

	if( m_State == Running )
		StartUpdates();
	else
		StopUpdateTimer();

	return *this;
}

time_t PPeriod::StartTime() const
{
	return system_clock::to_time_t( m_StartTime );
}

time_t PPeriod::TimeNow() const
{
	return system_clock::to_time_t( system_clock::now() );
}

void PPeriod::Synchronize( const time_t& a_Start, const time_t& a_Sent, const unsigned long& a_Elapsed )
{	// (re)sets internal values - only if it can read the strings

	// Set the start time - fairly straightforward
	m_StartTime = system_clock::from_time_t( a_Start );

	// set the elapsed time
	// if we are running (we assume the state has been adjusted already)
	// take into account the time taken for the message to arrive
	// and reset the TimeCheckRemainder (fwiw!) and the TimerStart
	// TODO good enough, but maybe it could be made accurate ?!
	// (probably the biggest error could be the realtime clock difference between the 2 devices)

	seconds NewMatchTime( a_Elapsed );

	if( m_State == Running )
	{
		system_clock::time_point TimeNow( system_clock::now() );
		system_clock::time_point SentTime( system_clock::from_time_t( a_Sent ) );
		seconds Delay = duration_cast<seconds>( TimeNow - SentTime );
		if( Delay > seconds::zero() )
		{
			NewMatchTime += Delay;
		}
		m_AccumulatedTime.m_TimeCheckRemainder = microseconds::zero();
		m_AccumulatedTime.m_TimerStart = system_clock::now();
	}

	if( NewMatchTime != m_AccumulatedTime.m_MatchSeconds )
	{
		m_AccumulatedTime.m_MatchSeconds = NewMatchTime;
		TimeChanged();
	}

}

ETimerState PPeriod::State() const
{
	return m_State;
}

seconds PPeriod::RemainingTime() const
{
	return m_Settings.m_Duration - ElapsedTime();
}

seconds PPeriod::ElapsedTime() const
{
	if( m_State == Running )
	{
		const_cast<PPeriod*>(this)->AccumulateTime();
	}
	return m_AccumulatedTime.m_MatchSeconds;
}

// Controls -----------------------------------
void PPeriod::SetReady()
{
	if( m_State == Ready )
		return;

	StopUpdateTimer();
	m_StartTime = system_clock::time_point( system_clock::duration( 0 ) );
	m_AccumulatedTime.Clear();
	SetState( Ready );
	TimeChanged(); // n.b. must come after SetState(Ready) or the time gets updated !!
}

void PPeriod::Start()
{
	if( !CanStart() )
		return;

//	if( m_State == Finished )
//		Reset();

	m_AccumulatedTime.m_TimerStart = system_clock::now();
	m_StartTime = m_AccumulatedTime.m_TimerStart;
	StartUpdates();
	SetState( Running );
}

void PPeriod::UndoStart()
{
	if( m_State != Running )
		return;
	SetReady();
}

void PPeriod::Pause()
{
	assert( State()==Running && "Pause requested, but we're not running !" );
	if( !CanPause() )
		return;
	DoPause();
	SetState( Paused );
}

void PPeriod::UndoPause()
{
	if( m_State!=Paused )
		return;
	DoUndoPause();
	SetState( Running );
}

PPeriod::SAccumulatedTime PPeriod::Continue()
{
	assert( State()==Paused || State()==Timed_out && "Continue requested, but we're not Paused !" );
	if( !CanContinue() )
		return m_AccumulatedTime;
	const SAccumulatedTime CurrentTimes( m_AccumulatedTime );
	m_AccumulatedTime.m_TimerStart = system_clock::now();
	StartUpdates();
	SetState( Running );
	return CurrentTimes;
}

void PPeriod::UndoContinue(const SAccumulatedTime &a_OldTimes, const ETimerState a_OldState )
{ // Go back to paused state
	assert( State()==Running && "UndoContinue requested, but we're not running !" );
	DoPause();
	m_AccumulatedTime = a_OldTimes;
	SetState( a_OldState );
	TimeChanged();
}

bool PPeriod::Finish()
{
	if( ! CanFinish() )
		return false;

	const bool bWasPaused( m_State != Running );
	StopUpdateTimer();
	if( !bWasPaused )
	{	// We change the state before issuing the timeChanged signal
		// so that any delay in getting the time thereafter is not added in Time()
		m_State = Finished;
		if( AccumulateTime() )
		{
			TimeChanged();
		}
		StateChanged();
	}
	else // Paused
	{	// Just change the state
		SetState( Finished );
	}
	// Clear alarm (if set)
	Alarm().Acknowledge();
	return bWasPaused;
}

void PPeriod::UndoFinish(bool a_bRestartPause)
{
	if( m_State != Finished )
		return;

	if( a_bRestartPause )
	{ // Just change State
		SetState( Paused );
	}
	else
	{	// go back to running
		AccumulateTime();
		StartUpdates();
		SetState( Running );
	}
	// Ensure UI updates in either case
	TimeChanged();
}

void PPeriod::Reset()
{
	if( !CanReset() )
		return;
	StopUpdateTimer();
	m_StartTime = system_clock::time_point( system_clock::duration( 0 ) );
	m_AccumulatedTime.Clear();
	m_Alarm.Reset();
	SetState( Ready );
	TimeChanged();
}

// Whether controls will do anything or not
bool PPeriod::CanStart() const
{
	return m_State==Ready;
}
bool PPeriod::CanPause() const
{
	return m_State==Running;
}
bool PPeriod::CanContinue() const
{
	return m_State==Paused;
}
bool PPeriod::CanFinish() const
{
	return m_State==Running || m_State==Paused;
}
bool PPeriod::CanReset() const
{
	return true; //m_State==Finished;
}

void PPeriod::StartUpdates() const
{	// n.b. we check the time quite often
	// but we only pass on the update message once a second
	// (problems if boost signal is const !)
	const_cast<PPeriod*>(this)->StartUpdateTimer( 200 );
}

void PPeriod::UpdateTime()
{
	if( m_State != Running )
		return;

	// issue a signal if the seconds count changed
	// and check alarm
	if( AccumulateTime() )
	{
		TimeChanged();
		m_Alarm.Check(RemainingTime());
	}
}

void PPeriod::SetState( ETimerState a_State )
{
	if( m_State != a_State )
	{
		m_State = a_State;
		StateChanged();
	}
}

void PPeriod::DoPause()
{
	if( AccumulateTime() )
		TimeChanged();
	StopUpdateTimer();
}

void PPeriod::DoUndoPause()
{
	if( AccumulateTime() )
		TimeChanged();
	StartUpdates();
}

bool PPeriod::AccumulateTime()
{
	// Get the time since the last check / increment of seconds
	// accumulate any whole seconds
	// Note any remaining ticks
	// reset the StartTime
	const system_clock::time_point Time( system_clock::now() );
	microseconds Elapsed( m_AccumulatedTime.m_TimeCheckRemainder + duration_cast<microseconds>( Time - m_AccumulatedTime.m_TimerStart ) );
	seconds MoreSeconds( duration_cast<seconds>( Elapsed ) );
	m_AccumulatedTime.m_MatchSeconds += MoreSeconds;
	m_AccumulatedTime.m_TimeCheckRemainder = Elapsed - MoreSeconds;
	m_AccumulatedTime.m_TimerStart = Time;

	// issue a signal if the seconds count changed
	return MoreSeconds.count() > 0;

}


string PPeriod::DisplayTimeStr( const seconds a_Seconds )
{
	// Convert the seconds into "hh:mm:ss"
	// For negative time, display a + in front
	const hours H( duration_cast<hours>( a_Seconds ) );
	const minutes M( duration_cast<minutes>( a_Seconds - seconds( H ) ) );
	const seconds S( duration_cast<seconds>(a_Seconds)-seconds( H ) - seconds( M ) );

	ostringstream ss;
	if( a_Seconds.count() < 0 )
	{
		ss << "+";
	}
	if( H.count() != 0 )
	{
		ss << abs(H.count()) << ":";
	}
	ss.fill( '0' );
	ss << std::setw( 2 ) << abs(M.count()) << ":" << std::setw( 2 ) << abs(S.count());

	return ss.str();
}

string PPeriod::DisplayTimeStr( const time_t a_Time )
{ // Time as a localised string
// 	ostringstream ss;
// 	ss << std::put_time( std::localtime( &a_Time ), "%T" );
// 	return ss.str();
	std::tm * LTime( std::localtime( &a_Time ) );
	if( LTime == NULL )
		return string();
	char Buff[100];
	strftime( Buff, sizeof( Buff ), "%T", LTime );
	return string( Buff );
}
