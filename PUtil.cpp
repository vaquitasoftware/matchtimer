//
// Created by timot on 11/01/2021.
//

#include "PUtil.h"

std::ostream& WriteQuotedString( std::ostream& a_rOutStream, const std::string& a_rString, const string& a_Ends ) // = "\"\""
{
	a_rOutStream << a_Ends[0];
	a_rOutStream.write( a_rString.c_str(), a_rString.size() );
	a_rOutStream << a_Ends[1];
	return a_rOutStream;
}

std::istream& ReadQuotedString( std::istream& a_rInStream, std::string& a_rString, const string& a_Ends ) // = "\"\""
{
	// clear white
	a_rInStream >> ws;
	// next char _must_ be a "Start"
	if ( a_rInStream.peek() != a_Ends[0] )
	{	// indicate failure
		a_rInStream.setstate( ios::failbit );
		return a_rInStream;
	}

	a_rInStream.ignore(1); // skip the start

	a_rString.erase();

	// read one char at a time, until the "End" is found
	char Ch;
	a_rInStream.get( Ch );
	while( Ch  != a_Ends[1] && a_rInStream.good() )
	{
		a_rString += Ch;
		a_rInStream.get( Ch );
	}

	// stream will be fail() && eof() if one is never found...

	return a_rInStream;
}
