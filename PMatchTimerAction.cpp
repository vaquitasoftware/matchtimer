//
//  PMatchTimerAction.cpp
//  TeleScore
//
//  Created by Timothy Pitt on 03/03/2017.
//  Copyright © 2017 Timothy Pitt. All rights reserved.
//

#include "PMatchTimerAction.h"

#include "PMatchTimer.h"

PMatchTimerAction::PMatchTimerAction(unsigned long a_Index, EActionID a_EActionID, PMatchTimer& a_Match )
: PAction( a_Index ),
m_EActionID( a_EActionID ),
m_Match( a_Match )
{}

