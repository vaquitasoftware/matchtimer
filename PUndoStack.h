//
//  PUndoStack.h
//  TeleScore
//
//  Created by Timothy Pitt on 06/02/2017.
//  Copyright © 2017 Timothy Pitt. All rights reserved.
//


#ifndef PUndoStack_h
#define PUndoStack_h

#include "PAction.h"

#include <list>


class PUndoStack
{
public:
	PUndoStack();
	PUndoStack( const PUndoStack& a_Other );
	PUndoStack& operator=( const PUndoStack& a_Other );
	~PUndoStack();
	
	void Clear();
	
	unsigned long NextIndex();
	unsigned long CurrentIndex() const;
	
	void Add( PAction * a_pAction, bool a_bSignal = true );
	
	void Undo( bool a_bSignal = true );
	void Redo( bool a_bSignal = true );

	bool CanUndo() const;
	bool CanRedo() const;
	
	PAction * Find( unsigned long a_Index );
	
	// derived classes to use for signals
	virtual void ActionDone(const PAction& a_Action, bool a_bRedo )
	{}
	
protected:
	typedef	std::list< PAction * > STLActionList;
	STLActionList m_UndoList;
	STLActionList::iterator m_Current;
	unsigned long m_NextIndex;
};


#endif /* PUndoStack_h */
