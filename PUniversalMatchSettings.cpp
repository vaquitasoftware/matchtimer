//
//  PUniversalMatchSettings
//  Period settings to define a type of match
//
//  Copyright © 2021 Timothy Pitt, Vaquita Software. All rights reserved.
//
/*
	This file is part of the MatchTimer core library.

	MatchTimer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MatchTimer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MatchTimer in the COPYING file.
	If not, see <https://www.gnu.org/licenses/>.
*/

#include "PUniversalMatchSettings.h"
#include "PUtil.h"

unsigned int PUniversalMatchSettings::PeriodCount() const
{
	return m_PeriodSettings.size();
}

PPlayPeriod::SPlaySettings PUniversalMatchSettings::PeriodAt(const unsigned int a_At) const
{
	STLPeriodsConstIter It( FindIterAt( a_At ) );
	if( It == m_PeriodSettings.end() )
		return PPlayPeriod::SPlaySettings();
	return *It;
}

void PUniversalMatchSettings::AddPeriod(const unsigned int a_Prev )
{ // Appends if a_Prev is not valid
	STLPeriodsConstIter Next( FindIterAt( a_Prev + 1 ) );
	m_PeriodSettings.insert( Next, PPlayPeriod::SPlaySettings() );
}

void PUniversalMatchSettings::SetPeriod(const unsigned int a_At, PPlayPeriod::SPlaySettings a_PeriodSettings )
{
	STLPeriodsIter It( FindIterAt( a_At ) );
	if( It != m_PeriodSettings.end() )
		*It = a_PeriodSettings;
}

void PUniversalMatchSettings::AppendPeriod(PPlayPeriod::SPlaySettings a_PeriodSettings)
{
	m_PeriodSettings.push_back(a_PeriodSettings);
}

//--------------------------

PUniversalMatchSettings::STLPeriodsIter PUniversalMatchSettings::FindIterAt(const unsigned int a_At )
{
	STLPeriodsIter It( m_PeriodSettings.begin() );
	unsigned int Count( a_At );
	while( Count > 0 && It != m_PeriodSettings.end() )
	{
		++It;
		--Count;
	}
	return It;
}

PUniversalMatchSettings::STLPeriodsConstIter PUniversalMatchSettings::FindIterAt(const unsigned int a_At ) const
{
	return const_cast<PUniversalMatchSettings*>(this)->FindIterAt(a_At);
}

void PUniversalMatchSettings::Save(ostream& a_Strm ) const
{
	a_Strm << 1 << " ";
	a_Strm << m_PeriodSettings.size() << " ";
	for(PUniversalMatchSettings::STLPeriodsConstIter It(m_PeriodSettings.begin()); It != m_PeriodSettings.end(); ++It )
	{
		a_Strm << *It << " ";
	}
}

void PUniversalMatchSettings::Load(istream &a_Strm )
{
	int Version;
	a_Strm >> Version;
	if( Version == 1 )
	{
		int Count(0);
		a_Strm >> Count;
		if( a_Strm.good() && Count < 100 )
		{
			m_PeriodSettings.resize( Count );
			for(PUniversalMatchSettings::STLPeriodsIter It(m_PeriodSettings.begin()); It != m_PeriodSettings.end(); ++It)
			{
				a_Strm >> *It;
			}
		}
	}
}

