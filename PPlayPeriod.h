//
//  PPlayPeriod
//  A period which can be interrupted by a pause or timeout
//
//  Copyright © 2020 Timothy Pitt. All rights reserved.
//
/*
	This file is part of the MatchTimer core library.

	MatchTimer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MatchTimer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MatchTimer in the COPYING file.
	If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "PPeriod.h"
#include "SignalDefs.h"

#include <iostream>
using std::ostream;
using std::istream;

#include<list>
using std::list;
typedef list< PPeriod > TRestPeriodList;

class PPlayPeriod :	public PPeriod
{
public:
	struct SPlaySettings
	{
		SSettings m_PlayPeriod;
		SSettings m_TimeoutPeriod;
		SSettings m_BreakPeriod;
		string m_PauseName;

		SPlaySettings( const SSettings a_PlayPeriod = SSettings(), const SSettings a_TimeoutPeriod = SSettings(),
						const SSettings a_BreakPeriod = SSettings(), const string& a_PauseName = string() )
			: m_PlayPeriod(a_PlayPeriod),
			m_TimeoutPeriod(a_TimeoutPeriod),
			m_BreakPeriod(a_BreakPeriod),
			m_PauseName( a_PauseName )
		{}

		bool Valid() const
		{ return m_PlayPeriod.Valid(); }

		friend ostream& operator << ( ostream& a_Strm, const SPlaySettings& a_Val )
		{
			a_Strm << 1 << " ";
			a_Strm << a_Val.m_PlayPeriod << " " << a_Val.m_TimeoutPeriod << " " << a_Val.m_BreakPeriod << " ";
			WriteQuotedString( a_Strm, a_Val.m_PauseName );
			return a_Strm;
		}
		friend istream& operator >> ( istream& a_Strm, SPlaySettings& a_Val )
		{
			int Version;
			a_Strm >> Version;
			if( Version == 1 )
			{
				a_Strm >> a_Val.m_PlayPeriod >> a_Val.m_TimeoutPeriod >> a_Val.m_BreakPeriod;
				ReadQuotedString( a_Strm, a_Val.m_PauseName );
			}
			return a_Strm;
		}
	};

	PPlayPeriod( const SPlaySettings& a_Settings );
	PPlayPeriod( const PPlayPeriod& a_Other );
	virtual ~PPlayPeriod();

	// The UI wants to see both Periods
	// Hmm - possibly we could remove this,
	// and pass a (non-const) pRestPeriod
	// in the RestChanged signal.
	PPeriod * RestPeriod();

	// Controls
	virtual void Start();
	virtual void UndoStart();
	virtual void Pause();
	virtual void UndoPause();
	virtual PPeriod::SAccumulatedTime Continue();
	virtual void UndoContinue(const SAccumulatedTime &a_OldTimes, const ETimerState a_OldState);
 	virtual bool Finish();
	virtual void UndoFinish(bool a_bRestartPause);
	virtual void Reset();

	virtual bool CanContinue() const;

	bool Timeout();
	void UndoTimeout(bool a_bRestartPause);
	void FinishRest(); // Stop Rest period, before going to next period
	void UndoFinishRest();
	
	bool CanTimeout() const;

	// Signals
	TSignal RestChanged;

	virtual void UpdateTime();

private:
	bool EnsureLastRestStopped();

	const SSettings m_TimeoutSettings;
	const SSettings m_BreakSettings;
	string m_PauseName;

	TRestPeriodList m_RestPeriods; // i.e. all non-playing periods
};

