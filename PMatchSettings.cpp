//
//  PMatchSettings
//  ABC for settings to define Matches as a series of PlayPeriods
//  Also includes factory Create() with derived class ID enums
//  It's a bit ugly but keeps things in one place when you add a new class
//
//  Copyright © 2021 Timothy Pitt, Vaquita Software. All rights reserved.
//
/*
	This file is part of the MatchTimer core library.

	MatchTimer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MatchTimer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MatchTimer in the COPYING file.
	If not, see <https://www.gnu.org/licenses/>.
*/

#include "PMatchSettings.h"

#include "PBasicMatchSettings.h"
#include "PUniversalMatchSettings.h"

void PMatchSettings::Write(ostream& a_Strm, const PMatchSettings& a_Val )
{
	a_Strm << 1 << " ";
	a_Strm << a_Val.ID() << " ";
	WriteQuotedString( a_Strm, a_Val.Name() );
	a_Val.Save( a_Strm );
}

PMatchSettings * PMatchSettings::Read(istream &a_Strm )
{
	PMatchSettings * pSettings = nullptr;
	int Version;
	a_Strm >> Version;
	if( Version == 1 )
	{
		unsigned int ID( EUnknown );
		a_Strm >> ID;
		string Name;
		ReadQuotedString(a_Strm, Name);
		pSettings = PMatchSettings::Create( ID );
		if( pSettings )
		{
			pSettings->SetName( Name );
			pSettings->Load(a_Strm);
		}
	}
	return pSettings;
}

PMatchSettings * PMatchSettings::Create(unsigned int a_ID)
{
	switch( a_ID )
	{
		case EBasicMatchSettingsID:
			return new PBasicMatchSettings();
		case EUniversalMatchSettingsID:
			return new PUniversalMatchSettings();
		default:
			return nullptr;
	}
}
