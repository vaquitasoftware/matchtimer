//
//  PMatchTimerAction.h
//  TeleScore
//
//  Created by Timothy Pitt on 02/03/2017.
//  Copyright © 2017 Timothy Pitt. All rights reserved.
//

#pragma once

#include "PAction.h"

#include "EActionID.h"

class PMatchTimer;

class PMatchTimerAction : public PAction
{
public:
	PMatchTimerAction(unsigned long a_Index, EActionID a_EActionID, PMatchTimer& a_Match );

	EActionID ActionID() const { return m_EActionID; }
	const PMatchTimer& Match() const { return m_Match; }

protected:
	const EActionID m_EActionID;
	PMatchTimer& m_Match;
	
private:

};
