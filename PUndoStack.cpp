//
//  PUndoStack.cpp
//  TeleScore
//
//  Created by Timothy Pitt on 06/02/2017.
//  Copyright © 2017 Timothy Pitt. All rights reserved.
//

#include "PUndoStack.h"

#include <algorithm>
#include <boost/bind.hpp>

#include <assert.h>

PUndoStack::PUndoStack()
{
	m_Current = m_UndoList.end();
	m_NextIndex = 1;
}

PUndoStack::PUndoStack( const PUndoStack& a_Other )
{
	// Do nothing in fact ! Leave the stack empty
	m_Current = m_UndoList.end();
	m_NextIndex = 1;
}

PUndoStack& PUndoStack::operator=( const PUndoStack& a_Other )
{
	if( this == &a_Other )
		return *this;
	
	Clear();
	
	return *this;
}

PUndoStack::~PUndoStack()
{
	Clear();
}

void PUndoStack::Clear()
{
	for( STLActionList::iterator It = m_UndoList.begin(); It != m_UndoList.end(); ++It )
	{
		delete *It;
	}
	m_UndoList.clear();
	m_Current = m_UndoList.end();
	m_NextIndex = 1;
}

unsigned long PUndoStack::NextIndex()
{	// TODO - design this indexing. This will do for now - at least the numbers are unique. For a while ;-)
	// Use CurrentIndex() + 1 ? NO !!
	assert( m_NextIndex != 0 && "Action index overflow !" );
	return m_NextIndex++;
}

unsigned long PUndoStack::CurrentIndex() const
{
	if( m_Current != m_UndoList.end() )
		return (*m_Current)->Index();
	return 0;
}

void PUndoStack::Add( PAction * a_pAction, bool a_bSignal )
{	// Keep the Action for myself - TODO use a smart pointer... :-)
	
	assert( a_pAction );
	
	// erase anything above the current position
	// push...
	// do the action
	if( ! m_UndoList.empty() && *m_Current != m_UndoList.back() )
	{
		if( m_Current != m_UndoList.end() )
		{
			++m_Current;
		}
		else
		{	// m_Current was at end(), all actions were undone
			// so delete the entire stack			
			m_Current = m_UndoList.begin();
		}
		for( STLActionList::iterator It(m_Current); It != m_UndoList.end(); ++It )
		{
			delete *It;
		}
		m_UndoList.erase( m_Current, m_UndoList.end() );
	}
	
	m_UndoList.push_back( a_pAction );
	m_Current = m_UndoList.end();
	--m_Current;
	
	PAction * pAction( *m_Current );
	if( pAction )
	{
		pAction->Redo();
		if( a_bSignal )
			ActionDone( *pAction, true );
	}
}

void PUndoStack::Undo( bool a_bSignal )
{
	if( CanUndo() )
	{
		PAction * pAction( *m_Current );
		if( m_Current != m_UndoList.begin() )
		{
			--m_Current;
		}
		else
		{
			m_Current = m_UndoList.end();
		}
		if( pAction )
		{
			pAction->Undo();
			if( a_bSignal )
				ActionDone( *pAction, false );
		}
	}
}

void PUndoStack::Redo( bool a_bSignal )
{
	if( CanRedo() )
	{
		++m_Current;
		PAction * pAction( *m_Current );
		if( pAction )
		{
			pAction->Redo();
			if( a_bSignal )
				ActionDone( *pAction, true );
		}
	}
}

bool PUndoStack::CanUndo() const
{
	return !m_UndoList.empty() && m_Current != m_UndoList.end();
}

bool PUndoStack::CanRedo() const
{
	if( m_UndoList.empty() )
		return false;
	auto Back( m_UndoList.end() );
	--Back;
	return m_Current != Back;
}

PAction * PUndoStack::Find( unsigned long a_Index )
{
	STLActionList::iterator It = std::find_if(m_UndoList.begin(), m_UndoList.end(),
																					 boost::bind(&PAction::Index, _1) == a_Index );
	if( It == m_UndoList.end() )
		return NULL;
	
	return *It;
}







